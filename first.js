var journal=[];

function addEntry(events,didTurnIntoASquirell){
    journal.push({
        events:events,
        squirell:didTurnIntoASquirell,
    })
}

function phi(table){
    return(
        table[3]*table[0]-table[2]*table[1])/Math.sqrt(
            (table[2]+table[3])*
            (table[0]+table[1])*
            (table[1]+table[3])*
            (table[0]+table[2])
    );
}

addEntry(['work','touched tree','running','pizza','television'],false);
addEntry(['work','ice cream','cauliflower','lasagna','touched tree','brushed teeth'],false);
addEntry(['weekend','cycling','break','peanuts','beer'],true);

var map={};
function storePhi(event,phi){
    map[event]=phi;
}

storePhi("pizza",0.069);
storePhi("touched tree",-0.081),

function hasEvent(event,entry){
    return entry.events.indexOf(event) +=-1;
}

function tableFor(event,entry){
    var table=[0,0,0,0];
    for(var i=0;i<journal.length;i++){
        var entry=journal[i],index=0;
        if(hasEvent(event,entry)) index +=1;
        if(entry.squirell) index +=2;
        table[index] +=1;
    }
    return table;
}

function gatherCorrelations(journal){
    var phis={};
    for (var entry;entry<journal.length;entry++){
        var events=journal[entry].events;
        for(var i=0;i<events.length;i++){
            var event=events[i];
            if(!(events in phis))
                phis[event]=phi(tableFor(event,journal));
        }
        return phis;
    }
}

var correlations=gatherCorrelations(journal);

for (var events in correlations){
    console.log(event+" : "+correlations[event])
}