function Rabbit(type){
    this.type=type;
}

var killerRabbit=new Rabbit("Killer");
var blackRabbit=new Rabbit("Black");

rabbitList=[killerRabbit,blackRabbit];

rabbitList.forEach(element => {
    console.log(element.type);
});